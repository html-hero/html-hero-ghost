FROM ghost:2.20.1

RUN npm install --save ghost-google-cloud-storage
COPY content/adapters/storage/gcloud content/adapters/storage/gcloud
# workaround since there's no update on npm
COPY ./ghost-google-cloud-storage/index.js node_modules/ghost-google-cloud-storage/index.js

COPY ./key.json ./key.json

COPY content/themes/casper/default.hbs versions/2.20.1/content/themes/casper/default.hbs

COPY mail/template/invite-user.html versions/2.20.1/core/server/services/mail/templates/invite-user.html

